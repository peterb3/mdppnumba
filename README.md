# Setup

Install the required packages with:  
`pip install -r requirements.txt`

# Exercises

[GameOfLife](Workshop/GameOfLife.ipynb)
* Learn the fundamentals of Numba
* Have fun simulating The Game Of Life

[Lists](Workshop/Lists.ipynb)
* Learn how to use lists in Numba
* Observe how Numba implements typed lists
* Compare the performance to a padded Numpy array
* See how Numba interacts with types other than Numpy arrays

[Prange](Workshop/Prange.ipynb)
* Learn more about parallelism in Numba
* See the order in which a 'prange' loop executes
* Examine the overhead of using 'prange'

# Numpy

Don't worry if you have not used Numpy before, you do not need to be an expert to do this workshop.   
There are two useful commands that you will need to know:  

`np.zeros(shape)` returns an array of the given shape, filled with zeros  
ref: https://docs.scipy.org/doc/numpy/reference/generated/numpy.zeros.html

`np.random.random()` returns a random float in the half-open interval [0.0, 1.0)  
ref: https://docs.scipy.org/doc/numpy-1.15.0/reference/generated/numpy.random.random.html

Please ask if you have any more questions!


